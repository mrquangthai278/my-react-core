import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import classnames from 'classnames';
import Sidebar from '../components/shared/Sidebar';
import Header from '../components/shared/Header';
import actions from '../redux/actions';
import './DashBoardLayout.scss';

const DashboardPage = props => {
  const dispatch = useDispatch();
  const isMenuExpanded = useSelector(state => state.user.isMenuExpanded);

  const _getClassWrapper = () => {
    return classnames('wrap-screen', {
      'wrap-screen__expanded': isMenuExpanded
    });
  };

  const callApi = () => {
    dispatch(actions.user.getListUser());
  };

  useEffect(() => {
    console.log('Starting here', process.env.API_URL);
  });

  return (
    <div className='wrap-dashboard'>
      <Sidebar />
      <div className={_getClassWrapper()}>
        <Header />
        <div className='wrap-container'>{props.children}</div>
      </div>
    </div>
  );
};

export default DashboardPage;
