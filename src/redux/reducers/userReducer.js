import { GET_LIST_USER } from '../constants/user';

const stateDefault = {
  listUser: [],
  isMenuExpanded: true
};

export const userReducer = (state = stateDefault, action) => {
  switch (action.type) {
    case GET_LIST_USER:
      state.listUser = action.data;
      return { ...state };
    case `TOOGLE_MENU`:
      state.isMenuExpanded = !state.isMenuExpanded;
      return { ...state };
    default:
      return state;
  }
};
