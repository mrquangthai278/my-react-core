import { createStore, applyMiddleware, compose } from 'redux';
import { rootReducer } from './reducers/rootReducer';
import { loadStateFromLocal, saveStateToLocal } from '../plugins/localStorage';
import thunk from 'redux-thunk';

const persistedState = loadStateFromLocal();

const store = createStore(
  rootReducer,
  persistedState,
  compose(applyMiddleware(thunk))
);

store.subscribe(() => {
  saveStateToLocal(store.getState());
});
export default store;
