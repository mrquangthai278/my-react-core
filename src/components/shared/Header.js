import React, { Fragment, useState } from 'react';
import classnames from 'classnames';
import './Header.scss';
import iconCalendar from '../../assets/images/icon/calendar.png';
import iconHome from '../../assets/images/icon/home.png';
import iconVn from '../../assets/images/icon/vn.png';
import iconRing from '../../assets/images/icon/ring.png';
import iconAvatar from '../../assets/images/default/avatar.png';

const HeaderLayout = props => {
  const isMobile = () => process.browser && window.innerWidth < 500;

  const _getClassPopover = () => {
    if (!isMobile()) return classnames('item__popover');
    return classnames('item__popover', { 'item__popover--right': isMobile() });
  };

  return (
    <Fragment>
      <div className='wrap-header'>
        <div className='block-input'>
          <i className='fas fa-search'></i>
          <input type='text' />
        </div>
        <div className='block-info'>
          <div className='item'>
            <div className='item__icon'>
              <img src={iconHome} />
            </div>
            <div className='item__desc'>
              <p>Head Office</p>
              <p>9999</p>
            </div>
            {isMobile() ? (
              <div className='item__popover'>
                <p>Head Office</p>
                <p>9999</p>
              </div>
            ) : (
              ''
            )}
          </div>
          <div className='item'>
            <div className='item__icon'>
              <img src={iconCalendar} />
            </div>
            <div className='item__desc'>
              <p>Working Date</p>
              <p>31/10/2019</p>
            </div>
            {isMobile() ? (
              <div className={_getClassPopover()}>
                <p>Working Date</p>
                <p>31/10/2019</p>
              </div>
            ) : (
              ''
            )}
          </div>
          <div className='item cursor-pointer '>
            <div className='item__icon'>
              <img src={iconVn} />
            </div>
            <div className='item__desc'>
              <p>Tiếng Việt</p>
            </div>
            <div className='item__desc'>
              <i className='fas fa-angle-down'></i>
            </div>
            <div className={_getClassPopover()}>
              <span>Tiếng Anh</span>
            </div>
          </div>
          <div className='item cursor-pointer'>
            <div className='item__icon'>
              <img src={iconRing} />
            </div>
            <div className='item__desc'>
              <i className='fas fa-angle-down'></i>
            </div>
            <div className={_getClassPopover()}>
              <span>Notification</span>
            </div>
          </div>
          <div className='item cursor-pointer'>
            <div className='item__desc'>
              <p>vinhtl@jits.vn</p>
            </div>
            <div className='item__icon'>
              <img src={iconAvatar} />
            </div>
            <div className='item__desc'>
              <i className='fas fa-angle-down'></i>
            </div>
            <div className='item__popover item__popover--right'>
              <span>Logout</span>
            </div>
          </div>
        </div>
      </div>
    </Fragment>
  );
};

export default HeaderLayout;
