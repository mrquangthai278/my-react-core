import React, { Fragment, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import classnames from 'classnames';
import listMenuData from './../../static/menu';
import './Sidebar.scss';

const initialListMenu = () => {
  const tempArr = listMenuData;
  tempArr.map(item => {
    item.isActive = false;
  });
  return tempArr;
};
const SidebarLayout = props => {
  const dispatch = useDispatch();
  const isMenuExpanded = useSelector(state => state.user.isMenuExpanded);

  const [listMenu, setListMenu] = useState(initialListMenu);

  const toogleSidebar = () => {
    dispatch({ type: `TOOGLE_MENU` });
  };

  const _getClassMenu = () => {
    return classnames('block-menu', { 'is-hidden': isMenuExpanded });
  };

  const _getClassTitleMenu = () => {
    return classnames('title--text', 'cursor-pointer', {
      'is-hidden': isMenuExpanded
    });
  };

  const _getClassWrapper = () => {
    return classnames('wrap-sidebar', {
      'wrap-sidebar__mini': isMenuExpanded
    });
  };

  const _getClassBlockTitle = () => {
    return classnames('block-title', {
      'block-title__mini': isMenuExpanded
    });
  };

  const handleClickMenu = menu => {
    let cloneListMenu = [...listMenu];
    const index = cloneListMenu.findIndex(item => item.key === menu.key);
    cloneListMenu[index].isActive = !cloneListMenu[index].isActive;
    setListMenu(cloneListMenu);
  };

  const _getClassSubMenu = menu => {
    return classnames('item--list-submenu', { 'is-active': menu.isActive });
  };

  return (
    <Fragment>
      <div className={_getClassWrapper()}>
        <div className={_getClassBlockTitle()}>
          <div className={_getClassTitleMenu()}>
            <span>OPTIMAL 9</span>
          </div>
          <div
            className='title--icon cursor-pointer'
            onClick={() => {
              toogleSidebar();
            }}>
            <i className='fas fa-align-justify'></i>
          </div>
        </div>
        <div className={_getClassMenu()}>
          {listMenu.map(item => {
            return (
              <div className='item' key={item.key}>
                <div
                  className='item--main'
                  onClick={() => {
                    handleClickMenu(item);
                  }}>
                  <div className='item--main__label'>
                    <i className={item.icon || `fas fa-cloud`}></i>
                    <span>{item.text}</span>
                  </div>
                  {item.subMenu.length ? (
                    <div className='item--main__sub'>
                      <i
                        className={
                          item.isActive
                            ? 'fas fa-angle-up'
                            : 'fas fa-angle-down'
                        }></i>
                    </div>
                  ) : (
                    ''
                  )}
                </div>
                <div className={_getClassSubMenu(item)}>
                  {item.subMenu.map(subItem => {
                    return (
                      <div className='submenu-item' key={subItem.key}>
                        <span>{subItem.text}</span>
                      </div>
                    );
                  })}
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </Fragment>
  );
};

export default SidebarLayout;
