import React, { Fragment, useState } from 'react';
import { Button } from 'antd';
import './Button.scss';

const ButtonLayout = props => {
  const [count, setCount] = useState(false);

  return (
    <Fragment>
      <div className=''></div>
      <Button type={props.type || 'primary'} onClick={props.onClick}>
        {props.label || 'Button'}
      </Button>
    </Fragment>
  );
};

export default ButtonLayout;
