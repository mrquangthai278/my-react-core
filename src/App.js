import React from 'react';

//Router
import { BrowserRouter as Router, Route } from 'react-router-dom';

//Global Style
import './assets/styles/global.scss';

//Pages
import LoginPage from './pages/LoginPage';
import TabManagement from './pages/dashboard/TabManagement';
import DashBoardLayout from './layouts/DashBoardLayout';

const DashBoard = () => (
  <DashBoardLayout>
    <TabManagement />
  </DashBoardLayout>
);

const App = () => {
  return (
    <Router>
      <Route path='/' exact component={LoginPage}></Route>
      <Route path='/login' exact component={LoginPage}></Route>
      <Route path='/dashboard' exact component={DashBoard}></Route>
    </Router>
  );
};

export default App;
