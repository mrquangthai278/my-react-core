import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import ButtonLayout from '../components/shared/Button';
import actions from '../redux/actions';
import { useHistory } from 'react-router-dom';

const LoginPage = () => {
  const dispatch = useDispatch();
  const history = useHistory();
  const callApi = () => {
    history.push('/dashboard');
    dispatch(actions.user.getListUser());
  };

  useEffect(() => {
    console.log('Starting here', process.env.API_URL);
  });

  return (
    <div className='App'>
      <ButtonLayout onClick={callApi} />
    </div>
  );
};

export default LoginPage;
