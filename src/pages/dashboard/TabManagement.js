import React, { Fragment, useState, useEffect } from 'react';
import classnames from 'classnames';
import './TabManagement.scss';

const initialListTab = [
  {
    key: 1,
    list: [
      {
        key: 11,
        text: 'Customer Profile',
        icon: 'fa fa-cubes'
      },
      {
        key: 12,
        text: 'Customer Profile',
        icon: ''
      }
    ],
    activeKey: 11
  },
  {
    key: 2,
    list: [
      {
        key: 21,
        text: 'Customer Profile',
        icon: 'fa fa-cubes'
      },
      {
        key: 22,
        text: 'Customer Profile',
        icon: ''
      }
    ],
    activeKey: 22
  },
  {
    key: 3,
    list: [
      {
        key: 31,
        text: 'Customer Profile',
        icon: 'fa fa-cubes'
      },
      {
        key: 32,
        text: 'Customer Profile',
        icon: ''
      }
    ],
    activeKey: 32
  },
  {
    key: 4,
    list: [
      {
        key: 41,
        text: 'Customer Profile',
        icon: 'fa fa-cubes'
      },
      {
        key: 42,
        text: 'Customer',
        icon: ''
      },
      {
        key: 43,
        text: 'Customer',
        icon: ''
      },
      {
        key: 44,
        text: 'Customer',
        icon: 'fa fa-cubes'
      },
      {
        key: 45,
        text: 'Customer',
        icon: 'fa fa-cubes'
      },
      {
        key: 46,
        text: 'Customer',
        icon: 'fa fa-compass'
      },
      {
        key: 47,
        text: 'Customer',
        icon: 'fa fa-compass'
      },
      {
        key: 48,
        text: 'Customer',
        icon: ''
      }
    ],
    activeKey: 45
  }
];

const TabManagement = () => {
  const [listTab, setListTab] = useState([]);

  const _getClassMiniTab = (blockTab, tab) => {
    let tempBool = false;
    let cloneListTab = [...listTab];
    const index = cloneListTab.findIndex(item => item.key === blockTab.key);
    const listMiniTab = cloneListTab[index].list;
    listMiniTab.map((item, subIndex) => {
      if (subIndex !== 0 && item.key === cloneListTab[index].activeKey) {
        if (blockTab.key === cloneListTab[index].key) {
          if (listMiniTab[subIndex - 1].key === tab.key) tempBool = true;
        }
      }
    });
    return classnames(
      'item--mini-tab',
      {
        'item--mini-tab__active': tab.key === blockTab.activeKey
      },
      { 'no-border': tempBool }
    );
  };
  const handleRemoveTab = (e, blockTab, tab) => {
    e.stopPropagation();
    let cloneListTab = [...listTab];
    const index = cloneListTab.findIndex(item => item.key === blockTab.key);
    const subIndex = cloneListTab[index].list.findIndex(
      item => item.key === tab.key
    );
    cloneListTab[index].list = cloneListTab[index].list.filter(
      item => item.key !== tab.key
    );
    setListTab(cloneListTab);
  };

  const handleClickTab = (e, blockTab, tab) => {
    let cloneListTab = [...listTab];
    cloneListTab.map(item => {
      item.key === blockTab.key &&
        item.list.map(subItem => {
          if (subItem.key === tab.key) {
            item.activeKey = tab.key;
          }
        });
    });
    setListTab(cloneListTab);
  };

  useEffect(() => {
    setListTab(initialListTab);
  }, []);

  return (
    <Fragment>
      <section className='wrap-section-tabs'>
        <div className='block-list--tab'>
          {listTab.map(item => {
            return (
              <div className='item--tab' key={item.key}>
                <div className='item--tab__bar'>
                  {item.list.map(subItem => {
                    return (
                      <div
                        className={_getClassMiniTab(item, subItem)}
                        onClick={e => {
                          handleClickTab(e, item, subItem);
                        }}
                        key={subItem.key}>
                        <div className='item--mini-tab__main'>
                          <i
                            className={
                              subItem.icon
                                ? subItem.icon
                                : `fas fa-align-justify`
                            }></i>
                          <span>{subItem.text}</span>
                        </div>
                        {item.list.length < 5 ||
                        item.activeKey === subItem.key ? (
                          <div
                            className='item--mini-tab__extra'
                            onClick={e => {
                              handleRemoveTab(e, item, subItem);
                            }}>
                            <i className='fas fa-times'></i>
                          </div>
                        ) : (
                          ''
                        )}
                      </div>
                    );
                  })}
                </div>
                <div className='item--tab__screen'></div>
              </div>
            );
          })}
        </div>
      </section>
    </Fragment>
  );
};

export default TabManagement;
