//React
import React from 'react';
import ReactDOM from 'react-dom';
// import * as serviceWorker from './serviceWorker';

//Redux
import { Provider } from 'react-redux';
import store from './redux';

import App from './App';

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

// Learn more about service workers: https://bit.ly/CRA-PWA
// serviceWorker.unregister();
