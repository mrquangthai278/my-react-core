import Calendar from '../assets/images/icon/calendar.png';
import Home from '../assets/images/icon/home.png';
import Vn from '../assets/images/icon/vn.png';
import Ring from '../assets/images/icon/ring.png';
import Avatar from '../assets/images/default/avatar.png';

export default {
  iconHome: Home,
  iconCalendar: Calendar,
  iconVn: Vn,
  iconRing: Ring,
  defaultAvatar: Avatar
};
