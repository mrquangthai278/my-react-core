const listMenu = [
  {
    key: 1,
    icon: 'fa fa-cog',
    url: '',
    text: 'Voucher Management',
    subMenu: []
  },
  {
    key: 2,
    icon: 'fa fa-compass',
    url: '',
    text: 'Mortage',
    subMenu: [
      {
        key: 1,
        url: '',
        text: 'Example '
      },
      {
        key: 2,
        url: '',
        text: 'Example '
      },
      {
        key: 3,
        url: '',
        text: 'Example '
      }
    ]
  },
  {
    key: 3,
    icon: 'fa fa-compass',
    url: '',
    text: 'Treasury',
    subMenu: []
  },
  {
    key: 4,
    icon: 'fa fa-compass',
    url: '',
    text: 'System',
    subMenu: []
  },
  {
    key: 5,
    icon: 'fa fa-cubes',
    url: '',
    text: 'IFC',
    subMenu: []
  },
  {
    key: 6,
    icon: 'fa fa-deaf',
    url: '',
    text: 'Customer',
    subMenu: []
  },
  {
    key: 7,
    icon: 'fa fa-cubes',
    url: '',
    text: 'Deposit',
    subMenu: []
  },
  {
    key: 8,
    icon: 'fa fa-cubes',
    url: '',
    text: 'Credit',
    subMenu: [
      {
        key: 1,
        url: '',
        text: 'Example '
      },
      {
        key: 2,
        url: '',
        text: 'Example '
      },
      {
        key: 3,
        url: '',
        text: 'Example '
      }
    ]
  },
  {
    key: 9,
    icon: 'fa fa-cubes',
    url: '',
    text: 'Administration',
    subMenu: []
  },
  {
    key: 10,
    icon: 'fa fa-cubes',
    url: '',
    text: 'Accounting',
    subMenu: []
  },
  {
    key: 11,
    icon: 'fa fa-cubes',
    url: '',
    text: 'User Management',
    subMenu: [
      {
        key: 1,
        url: '',
        text: 'Example '
      },
      {
        key: 2,
        url: '',
        text: 'Example '
      },
      {
        key: 3,
        url: '',
        text: 'Example '
      }
    ]
  },
  {
    key: 12,
    icon: 'fa fa-cubes',
    url: '',
    text: 'Tien ich',
    subMenu: []
  }
];

export default listMenu;
