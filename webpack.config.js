const path = require('path');
const dotenv = require('dotenv');
const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = () => {
  const env = dotenv.config().parsed;

  const envKeys = Object.keys(env).reduce((prev, next) => {
    prev[`process.env.${next}`] = JSON.stringify(env[next]);
    return prev;
  }, {});

  return {
    mode: 'development',
    entry: './src/index.js',
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'js/bundle.js',
      publicPath: '/'
    },
    devServer: {
      contentBase: './src',
      historyApiFallback: true
    },
    resolve: {
      modules: [__dirname, 'node_modules'],
      alias: {},
      extensions: ['*', '.js', '.jsx']
    },
    module: {
      rules: [
        {
          test: /\.css$/,
          use: ExtractTextPlugin.extract({
            use: ['css-loader']
          })
        },
        {
          test: /\.scss$/,
          use: [
            {
              loader: 'style-loader'
            },
            {
              loader: 'css-loader'
            },
            {
              loader: 'sass-loader'
            }
          ]
        },
        {
          test: /\.js$/,
          exclude: /(node_modules|bower_components)/,
          use: {
            loader: 'babel-loader',
            options: {
              presets: ['react', 'stage-0']
            }
          }
        },
        {
          test: /\.(ttf|eot|svg|gif|png)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          use: [
            {
              loader: 'file-loader'
            }
          ]
        }
      ]
    },
    plugins: [
      new webpack.DefinePlugin(envKeys),
      new CleanWebpackPlugin(['dist']),
      new ExtractTextPlugin('css/bundle.css'),
      // new UglifyJSPlugin(),
      new HtmlWebpackPlugin({
        filename: 'index.html',
        template: 'src/index.html'
        // minify: {
        //     collapseWhitespace: true,
        //     removeAttributeQuotes: true
        // }
      }),

      // new CopyWebpackPlugin([{from: './app/icon', to: './icon'}]),
      new CopyWebpackPlugin([{ from: './src/assets', to: './assets' }]),
      new CopyWebpackPlugin([
        { from: './src/.htaccess', to: './.htaccess', toType: 'file' }
      ])
    ]
  };
};
